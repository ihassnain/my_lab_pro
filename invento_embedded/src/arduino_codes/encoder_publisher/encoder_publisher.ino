#include <ros.h>
#include <std_msgs/Int64.h>

ros::NodeHandle nh;
std_msgs::Int64 left_encoder_count;
std_msgs::Int64 right_encoder_count;

// left
#define encoder1PinA 2
#define encoder1PinB 3
//right
#define encoder2PinA 4
#define encoder2PinB 5

volatile long encoderLPos = 0;
volatile long encoderRPos = 0;
float dia = 61; //dia is in mm
float Dl, Dr, Dc, Ori_ch;
float ER = 2400; //Encoder resolution (pulses per revolution)
int x = 0;  //Initial x position of the robot
int y = 0;  //Initial y position of the robot
float Ori = 0;
float Pi = 3.14;
float baseRadius = 183; //wheelbase of the mobile robot in mm
float rotationL = 0;
float rotationR = 0;


ros::Publisher right_encoder("right_encoder", &right_encoder_count);
ros::Publisher left_encoder("left_encoder", &left_encoder_count);
int left_ticks = 0;
int right_ticks = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(encoder1PinA, INPUT);
  digitalWrite(encoder1PinA, HIGH);
  pinMode(encoder1PinB, INPUT);
  digitalWrite(encoder1PinB, HIGH);
  pinMode(encoder2PinA, INPUT);
  digitalWrite(encoder2PinA, HIGH);
  pinMode(encoder2PinB, INPUT);
  digitalWrite(encoder2PinB, HIGH);
  attachInterrupt(digitalPinToInterrupt(encoder1PinA), doEncoder1A, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoder1PinB), doEncoder1B, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoder2PinA), doEncoder2A, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoder2PinB), doEncoder2B, CHANGE);
  nh.initNode();
  nh.advertise(right_encoder);
  nh.advertise(left_encoder);
  // Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  // rotationL = encoderLPos/ER;
  // rotationR = encoderRPos/ER;
  rotationL = encoderLPos;
  rotationR = encoderRPos;
  
  Serial.print("L: ");
  Serial.print(rotationL);
  Serial.print(" R: ");
  Serial.println(rotationR);

  left_encoder_count.data = encoderLPos;
  right_encoder_count.data = encoderRPos;
  left_encoder.publish(&left_encoder_count);
  right_encoder.publish(&right_encoder_count);
  nh.spinOnce();
  

//   Dl = Pi * dia * rotationL;
//   Dr = Pi * dia * rotationR;
//   Dc = (Dl + Dr)/2;
//   Ori_ch = (Dr - Dl)/baseRadius ;
//   Ori = Ori + Ori_ch;
//   x = x+Dc*cos(Ori);
//   y = y+Dc*sin(Ori);
  
}

void doEncoder1A() {
  // look for a low-to-high on channel A
  if (digitalRead(encoder1PinA) == HIGH) {

    // check channel B to see which way encoder is turning
    if (digitalRead(encoder1PinB) == LOW) {
      encoderLPos = encoderLPos + 1;         // CW
    }
    else {
      encoderLPos = encoderLPos - 1;         // CCW
    }
  }

  else   // must be a high-to-low edge on channel A
  {
    // check channel B to see which way encoder is turning
    if (digitalRead(encoder1PinB) == HIGH) {
      encoderLPos = encoderLPos + 1;          // CW
    }
    else {
      encoderLPos = encoderLPos - 1;          // CCW
    }
  }
}

void doEncoder1B() {
  // look for a low-to-high on channel B
  if (digitalRead(encoder1PinB) == HIGH) {

    // check channel A to see which way encoder is turning
    if (digitalRead(encoder1PinA) == HIGH) {
      encoderLPos = encoderLPos + 1;         // CW
    }
    else {
      encoderLPos = encoderLPos - 1;         // CCW
    }
  }

  // Look for a high-to-low on channel B

  else {
    // check channel B to see which way encoder is turning
    if (digitalRead(encoder1PinA) == LOW) {
      encoderLPos = encoderLPos + 1;          // CW
    }
    else {
      encoderLPos = encoderLPos - 1;          // CCW
    }
  }
}


void doEncoder2A() {
  // look for a low-to-high on channel A
  if (digitalRead(encoder2PinA) == HIGH) {

    // check channel B to see which way encoder is turning
    if (digitalRead(encoder2PinB) == LOW) {
      encoderRPos = encoderRPos + 1;         // CW
    }
    else {
      encoderRPos = encoderRPos - 1;         // CCW
    }
  }

  else   // must be a high-to-low edge on channel A
  {
    // check channel B to see which way encoder is turning
    if (digitalRead(encoder2PinB) == HIGH) {
      encoderRPos = encoderRPos + 1;          // CW
    }
    else {
      encoderRPos = encoderRPos - 1;          // CCW
    }
  }
}

void doEncoder2B() {
  // look for a low-to-high on channel B
  if (digitalRead(encoder2PinB) == HIGH) {

    // check channel A to see which way encoder is turning
    if (digitalRead(encoder2PinA) == HIGH) {
      encoderRPos = encoderRPos + 1;         // CW
    }
    else {
      encoderRPos = encoderRPos - 1;         // CCW
    }
  }

  // Look for a high-to-low on channel B

  else {
    // check channel B to see which way encoder is turning
    if (digitalRead(encoder2PinA) == LOW) {
      encoderRPos = encoderRPos + 1;          // CW
    }
    else {
      encoderRPos = encoderRPos - 1;          // CCW
    }
  }
}

