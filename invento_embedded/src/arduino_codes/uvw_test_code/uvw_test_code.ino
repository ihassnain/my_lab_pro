
int lu = 25;
int lv = 27;
int lw = 29;
int ru = 45;
int rv = 43;
int rw = 41;

int renc = 0, lenc = 0;

int right_now, left_now;

int store_current(int upin, int vpin, int wpin) {
  int u = digitalRead(upin);
  int v = digitalRead(vpin);
  int w = digitalRead(wpin);

  int ret = (u << 2) | (v << 1) | w;

  return ret;
}

int compare(int now, int old) {
  int change = (now << 3) | old;

  if (
    change == 0b100110 ||
    change == 0b110010 ||
    change == 0b010011 ||
    change == 0b011001 ||
    change == 0b001101 ||
    change == 0b101100 ) {
    return 1;
  }

  else if (
    change == 0b100101 ||
    change == 0b101001 ||
    change == 0b001011 || 
    change == 0b011010 ||
    change == 0b010110 ||
    change == 0b110100 ) {
      return -1;
    }

    else{
      return 0;
    }
}

void lcheck() {
  int now = store_current(lu, lv, lw);

  // compare now and old
  lenc -= compare(now, left_now);

  left_now = now;
}

void rcheck() {
  int now = store_current(ru, rv, rw);

  // compare now and old
  renc += compare(now, right_now);

  right_now = now;
}


void setup_lenc_pin(int pin) {
  pinMode(pin, INPUT);
  attachInterrupt(pin, lcheck, CHANGE);
}

void setup_renc_pin(int pin) {
  pinMode(pin, INPUT);
  attachInterrupt(pin, rcheck, CHANGE);
}

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  setup_lenc_pin(lu);
  setup_lenc_pin(lv);
  setup_lenc_pin(lw);

  setup_renc_pin(ru);
  setup_renc_pin(rv);
  setup_renc_pin(rw);

  right_now = store_current(ru, rv, rw);
  left_now = store_current(lu, lv, lw);
  Serial.println("Reseting");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("e\t" + String(lenc) + "\t" + String(renc));
//  lenc = 0;
//  renc =0;
 delay(100);
  }
