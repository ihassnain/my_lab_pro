/* bldc motors */

#include "PinsB10.h"

bool debug = true;
static int bldc_pwm = 18;
static int max_bldc_pwm = 80;
bool roboremo_drive = true;
bool listen_nuc = true ;
int right_bldc_pwm = bldc_pwm;
int left_bldc_pwm = bldc_pwm;
int turning_speed= 10;
int eyesReset = 50;                                                                                                                                                                                


long command = 0;


void print_message(String );
void setup_communication();
void setup_motors();
void setup_bldc_motors();
void enable_motors();
void enable_bldc_motors();
void forward();
void bldc_forward();
void bldc_apply_pwm();
void reverse();
void bldc_reverse();
void left();
void right();
void bldc_right();
void bldc_left();
void stop_motors();
void bldc_stop_motors();
void bldc_disable_motors();
void run_navigation_commands(int );
int exec_serial_commands();
void run_command(long );
void set_right_pwm(int );
void set_left_pwm(int );
void bldc_disable_motors();
void enable_bldc_motors();

long bt_baud_rate = 9600;
long serial_baud_rate= 115200;

void setup_communication()
{

  Serial.begin(serial_baud_rate);
  Serial3.begin(bt_baud_rate );
  Serial2.begin(bt_baud_rate );

  print_message("start with rover again");
}

void print_message(String msg)
{
  //Serial.println(msg);
  /* Serial1.println(msg); */
  /* Serial2.println(msg); */
  Serial3.println(msg);
}

void setup_motors()
{
  setup_bldc_motors();
}

void setup_bldc_motors()
{
  print_message("Setting up BLDC motors ");

  pinMode(leftEnable, OUTPUT);
  pinMode(leftBrake, OUTPUT);
  pinMode(left_direction, OUTPUT);
  pinMode(left_speed_pin, OUTPUT);

  pinMode(rightEnable, OUTPUT);
  pinMode(rightBrake, OUTPUT);
  pinMode(right_direction, OUTPUT);
  pinMode(right_speed_pin, OUTPUT);

  digitalWrite(rightEnable, HIGH);
  digitalWrite(leftEnable, HIGH);
}

void enable_motors()
{
  enable_bldc_motors();
}

void enable_bldc_motors()
{

  if (bldc_pwm > max_bldc_pwm)
  {
    bldc_pwm = max_bldc_pwm;
  }

  digitalWrite(rightEnable, LOW);
  digitalWrite(leftEnable, LOW);

  digitalWrite(leftBrake, HIGH);
  digitalWrite(rightBrake, HIGH);
}

void forward()
{
  bldc_forward();
}

void bldc_forward()
{
  digitalWrite(left_direction, HIGH);
  digitalWrite(right_direction, LOW);

  bldc_apply_pwm();
}

void bldc_apply_pwm()
{
  if (left_bldc_pwm > max_bldc_pwm)
  {
    left_bldc_pwm = max_bldc_pwm;
  }
  if (right_bldc_pwm > max_bldc_pwm)
  {
    right_bldc_pwm = max_bldc_pwm;
  }
  if (right_bldc_pwm < 0)
  {
    right_bldc_pwm = 0;
  }
  if (left_bldc_pwm < 0)
  {
    left_bldc_pwm = 0;
  }
  analogWrite(left_speed_pin, left_bldc_pwm);
  analogWrite(right_speed_pin, right_bldc_pwm);
}

void reverse()
{
  bldc_reverse();
}

void bldc_reverse()
{
  print_message("reverse - BLDC");

  digitalWrite(left_direction, LOW);
  digitalWrite(right_direction, HIGH);

  bldc_apply_pwm();
}

void left()
{
  bldc_left();
}

void right()
{
  bldc_right();
}

void bldc_right()
{
  print_message("right - BLDC");

  digitalWrite(left_direction, HIGH);
  digitalWrite(right_direction, HIGH);

  analogWrite(left_speed_pin, turning_speed);
  analogWrite(right_speed_pin, turning_speed);
}

void bldc_left()
{
  enable_bldc_motors();

  digitalWrite(left_direction, LOW);
  digitalWrite(right_direction, LOW);

  analogWrite(left_speed_pin, turning_speed);
  analogWrite(right_speed_pin, turning_speed);
}

void stop_motors()
{
  bldc_stop_motors();
}

void bldc_stop_motors()
{
  digitalWrite(left_speed_pin, LOW);
  digitalWrite(right_speed_pin, LOW);

  /* delay(1000); */
  bldc_disable_motors();
}

void bldc_disable_motors()
{
  digitalWrite(rightEnable, HIGH);
  digitalWrite(leftEnable, HIGH);

  digitalWrite(leftBrake, LOW);
  digitalWrite(rightBrake, LOW);
}

/*
 * Get user input
 */

int exec_serial_commands()
{
  command = 0;
  if (listen_nuc)
  {
    if (Serial.available())
    {
      command = Serial.parseInt();
    }
    /* bluetooth */
    if (Serial3.available())
    {
      command = Serial3.parseInt();
    }

    if (Serial2.available())
    {
      command = Serial2.parseInt();
    }

  }

  if (!listen_nuc)
  {
    if (Serial3.available())
    {
      if (Serial3.parseInt() == 426)
      {
        listen_nuc = true;
        print_message("Robot Unlocked");
      }

      if (Serial.available())
      {
        if (Serial.parseInt() == 426)
        {
          listen_nuc = true;
          print_message("Robot Unlocked");
        }
      }
    }
  }

  run_command(command);
  return command;
}

void run_command(long com)
{
  if (com == 0)
    return;

  print_message("");
  print_message("command: " + String(com) + " ");

 if(com==111){
    Serial3.println("rd");
    Serial2.println("rd");
    }

  if (com > 400 && com < 500)
  {
    run_navigation_commands(com);
  }

  if (com > 500 && com < 580)
  {
    com = com - 500;
    set_right_pwm(com);
  }

  if (com > 600 && com < 680)
  {
    com = com - 600;
    set_left_pwm(com);
  }

  if (com >= 960 && com <= 970)
  {
  // passing data to HC05
  Serial.println(com);



 if(com==967){
    Serial3.println("Clear Map Pressed");
    Serial3.println("Clearing Map!");
    Serial3.println("---------");
    }

  if(com==966){
    Serial3.println("Map Reset Pressed");
    Serial3.println("Allowing you to Remap!");
    Serial3.println("----------");    
    }
  if(com==965){
    Serial3.println("Mark Point Pressed");
    Serial3.println("Marking This Point for Patrol (if Record Points Turned ON)");
    Serial3.println("----------");
    } 
  if(com==964){
        Serial3.println("Pressed Get Current Point"); 
    }
     
}

//uv code
if (com == 2800)
{
  analogWrite(R1SW_pin, 255);
  print_message("Switching ON UV lights");
}

if (com == 2801)
{
  analogWrite(R1SW_pin, 0);
  print_message("Switching OFF UV lights");
}

if (com >= 10000 && com < 20000)
{
  Serial.println(com);
  }

  if (com >= 20000 && com <30000)
  {
  Serial.println(com);
  }

  if (com >= 30000 && com <40000)
  {
  Serial.println(com);
  }

  if (com >= 50000 && com <60000)
  {
  // passing data to HC05
  float curr_x = (com-55000)/100.0;
  Serial3.println("X:"+ String(curr_x));
  }

  if (com >= 60000 && com < 70000)
  {
  // passing data to HC05
  float curr_y = (com-65000)/100.0;
  Serial3.println("Y:"+ String(curr_y));
  Serial3.println("--------");
  }

  if (com >= 45000 && com <= 45360)
  {
  // passing data to HC05
  Serial3.println("A:"+ String(com-45000));
  }
  
  
  command = 0;
}

void set_right_pwm(int pwm)
{
  right_bldc_pwm = pwm;
  //print_message("right pwm: " + String(right_bldc_pwm));
}

void set_left_pwm(int pwm)
{
  left_bldc_pwm = pwm;
  //print_message("left pwm: " + String(left_bldc_pwm));
}



void run_navigation_commands(int com)
{

  com = com - 400;

  if (com != 3)
  {
    enable_motors();
  }

  switch (com)
  {

  case 1:
    print_message("forward");
    forward();
    break;

  case 2:
    print_message("left");
    left();
    break;

  case 3:
    print_message("Stop");
    stop_motors();
    break;

  case 4:
    print_message("right");
    right();
    break;

  case 5:
    print_message("reverse");
    reverse();
    break;

  case 11:
    Serial.println("411");
    print_message("starting navigation...");
    break;
  case 25:
    print_message("robot Locked \nit will not listen to any serial command\nuse 426 to unlock the robot\n\n");
    listen_nuc = false;   
    bldc_disable_motors();
    break;

  case 26:
    print_message("Robot Unlocked");
    enable_bldc_motors();
    listen_nuc = true;     
    break;

  case 27:
    print_message("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    break;
  
  } // end of switch
  
}

void eyes_reset() 
{
  pinMode(eyesReset, OUTPUT);  // EYE Arduino DUE RESET
  digitalWrite(eyesReset, LOW);
  delay(500);
  digitalWrite(eyesReset, HIGH);
}



void setup()
{
  analogWrite(R1SW_pin, 0);
  eyes_reset();
  setup_communication(); //
  setup_motors();
}

void loop()
{
   exec_serial_commands();
}
