#include "MPU9250.h"

MPU9250 IMU(Wire, 0x68);
StaticJsonDocument<200> doc;
int mpu_status;
int prec = 6;
int x_offset =  29; // hardcore values   it was 13, 17
int y_offset = 4;
int x_min, x_max, y_min, y_max;
float ang;
float update_compass() {
    float angle = (atan2((IMU.getMagY_uT() - y_offset ), (IMU.getMagX_uT() - x_offset)) * 180) / 3.14;
    angle = angle + 2 * 360.0;
    angle = (int)angle % 360;
    return angle;
}

void update_offsets() {
    if (IMU.getMagX_uT() < x_min) {
        x_min = (x_min + IMU.getMagX_uT()) / 2;
    }
    if (IMU.getMagX_uT() > x_max) {
        x_max = (x_max + IMU.getMagX_uT()) / 2;
    }
    if (IMU.getMagY_uT() < y_min) {
        y_min = (y_min + IMU.getMagY_uT()) / 2;
    }
    if (IMU.getMagY_uT() > y_max) {
        y_max = (y_max + IMU.getMagY_uT()) / 2;
    }

    x_offset = (x_min + x_max) / 2;
    y_offset = (y_min + y_max) / 2;
}


void clibrate_acc() {
    mpu_status = IMU.calibrateAccel();
    if (mpu_status) {
        float axb = IMU.getAccelBiasX_mss();
        float axs = IMU.getAccelScaleFactorX();
        float ayb = IMU.getAccelBiasY_mss();
        float ays = IMU.getAccelScaleFactorY();
        float azb = IMU.getAccelBiasZ_mss();
        float azs = IMU.getAccelScaleFactorZ();
        IMU.setAccelCalX(axb, axs);
        IMU.setAccelCalY(ayb, ays);
        IMU.setAccelCalZ(azb, azs);
    }

}

void clibrate_gyro() {

    mpu_status = IMU.calibrateGyro();
    if (mpu_status) {
        float gxb = IMU.getGyroBiasX_rads();
        float gyb = IMU.getGyroBiasX_rads();
        float gzb = IMU.getGyroBiasX_rads();
        IMU.setGyroBiasX_rads(gxb);
        IMU.setGyroBiasY_rads(gyb);
        IMU.setGyroBiasZ_rads(gzb);

    }

}

int calibrate_compass() {
    //  print_message("Started calibrating compass kindly rotate compass");
    x_offset = 0;
    y_offset = 0;
    x_min = 0;
    x_max = 0;
    y_min = 0;
    y_max = 0;
    int x = 600;  // no of calibration values
    while (x > 0) {
        IMU.readSensor();
        update_offsets();
        update_compass();
        x--;
            print_message("calibrating ..." + String(IMU.getMagX_uT()) + " " + String(IMU.getMagY_uT()) + String(x_offset) + " " + String(y_offset));
        delay(50);
        bldc_right();
    }
      print_message("offset values : " + String(x_offset) + " " + String(y_offset));
      bldc_stop_motors();
    return 1;
}

void mpu9250_setup(){


    // start communication with IMU
    mpu_status = IMU.begin();
    if (mpu_status < 0) {
        while (1) {}
    }


    // setting the accelerometer full scale range to +/-8G
    IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
    // setting the gyroscope full scale range to +/-500 deg/s
    IMU.setGyroRange(MPU9250::GYRO_RANGE_500DPS);
    // setting DLPF bandwidth to 20 Hz
    IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_20HZ);
    // setting SRD to 19 for a 50 Hz update rate
    IMU.setSrd(19);
    //calibrate_compass();
    clibrate_acc();
    clibrate_gyro();
}

void mpu9250_call(){
    // read the sensor
    IMU.readSensor();
    doc["ax"] = IMU.getAccelX_mss();
    doc["ay"] = IMU.getAccelY_mss();
    doc["az"] = IMU.getAccelZ_mss();

    doc["gx"] = IMU.getGyroX_rads();
    doc["gy"] = IMU.getGyroY_rads();
    doc["gz"] = IMU.getGyroZ_rads();
    ang = update_compass();
    doc["cyaw"] = ang;
    serializeJson(doc, Serial);
    Serial.println();
}


