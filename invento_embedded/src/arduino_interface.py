#!/usr/bin/env python
import serial
import rospy
from std_msgs.msg import String, Bool, Int32


class ArduinoInterface():
    def __init__(self):

        self.ros_init()
        #self.serial_init()

    def ros_init(self):
        rospy.init_node("arduino_interface", anonymous=True)
        self.rate = rospy.Rate(5)
        self.sub = rospy.Subscriber(
            "arduino_in", String, callback=self.recv_cmd, queue_size=1)
        self.lidar_sub = rospy.Subscriber(
            "lidar_running", Bool, callback=self.lidar_callback, queue_size=1)
        self.out_pub = rospy.Publisher("arduino_out", String, queue_size=1)
        self.in_pub = rospy.Publisher("motor_cmd", Int32, queue_size=1)
        self.cmd = []
        self.lidar_running = False

    def serial_init(self):
        port = rospy.get_param("arduino_interface/port")
        baud = rospy.get_param("arduino_interface/baud")
        timeout = rospy.get_param("arduino_interface/timeout")

        self.ser = None
        while(self.ser is None):
            try:
                self.ser = serial.Serial(port, baud, timeout=timeout)
            except Exception as e:
                print(e)
                self.ser = None

    def recv_cmd(self, data):
        self.cmd = data.data.strip("~").split("~")
        #print(self.cmd)

    def lidar_callback(self, data):
        if(data.data):
            self.lidar_running = "~901"
        else:
            self.lidar_running = "~900"

    def send_serial(self):
        while(self.cmd):
            data = self.cmd.pop()
            self.in_pub.publish(int(data))
        if(data):
            self.ser.write(data+"\r\n")

    def read_serial(self):
        #line = self.ser.readline().strip("~").strip("\n").strip("\r")
        for data in line.split("~"):
            self.out_pub.publish(data)

    def run(self):
        while(1):
            #self.read_serial()
            self.send_serial()
            self.rate.sleep()


if __name__ == '__main__':
    interface = ArduinoInterface()
    interface.run()
