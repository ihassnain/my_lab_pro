#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Int64MultiArray, MultiArrayDimension, Int64
from math import pi


class EncoderInterface():
    def __init__(self, right_topic, left_topic, encoder_ticks_per_rotation, encoder_topic):
        rospy.init_node("encoder_interface")

        rospy.Subscriber(left_topic, Int64, self.left_callback, queue_size=1)
        rospy.Subscriber(right_topic, Int64, self.right_callback, queue_size=1)

        self.pub = rospy.Publisher(
            encoder_topic, Int64MultiArray, queue_size=10)

        self.last_left_tick = 0
        self.last_right_tick = 0

        self.curr_left_tick = 0
        self.curr_right_tick = 0

        left = MultiArrayDimension()
        left.label = "left_ticks"
        right = MultiArrayDimension()
        right.label = "right_ticks"

        self.encoder_msg = Int64MultiArray()
        self.encoder_msg.layout.dim = [left, right]

        self.rate = rospy.Rate(10)

        while not rospy.is_shutdown():
            self.publish_ticks()
            self.rate.sleep()

    def left_callback(self, msg):
        self.curr_left_tick = msg.data

    def right_callback(self, msg):
        self.curr_right_tick = msg.data

    def publish_ticks(self):
        left_delta = self.curr_left_tick - self.last_left_tick
        right_delta = self.curr_right_tick - self.last_right_tick

        self.last_left_tick = self.curr_left_tick
        self.last_right_tick = self.curr_right_tick

        self.encoder_msg.data = [left_delta, right_delta]
        self.pub.publish(self.encoder_msg)


if __name__ == "__main__":
    right_topic = "right_encoder"
    left_topic = "left_encoder"
    encoder_topic = "encoder_ticks"
    encoder_ticks_per_rotation = rospy.get_param("encoder/ticks_per_rotation")
    encoder = EncoderInterface(right_topic, left_topic, encoder_ticks_per_rotation, encoder_topic)
