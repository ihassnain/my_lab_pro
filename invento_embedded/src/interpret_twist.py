#!/usr/bin/env python
from math import pi, floor, atan2, sqrt
import rospy
from tf import transformations
from std_msgs.msg import Int8, Float32, String, Bool
from nav_msgs.msg import Odometry, Path
from geometry_msgs.msg import PoseStamped, Twist
from sensor_msgs.msg import LaserScan

def send(data):
    cmd = ""
    for i in data:
        cmd += "~" + str(i)
    print cmd
    send_pub.publish(cmd)
    

def vel_callback(data):
        x = data.linear.x
        z = data.angular.z
        
        print(x,z)
        if(x>0):
            print('forward 401')
            send([401])
            
        elif(x<0):
            print('reverse 405')
            send([405])
          

        elif(z>0):
            print('left 402')
            send([402])
            

        elif(z<0):
            print('right 404')
            send([404])
        elif(x==0 and z==0):
            print('Key Release')
            send([403])

if __name__ == '__main__':
    vel_topic = "cmd_vel"
    rospy.init_node('interpret_twist', anonymous=True)
    rospy.Subscriber(vel_topic, Twist, vel_callback, queue_size=1)  
    send_pub = rospy.Publisher('arduino_in', String, queue_size=1)
    rospy.spin()
