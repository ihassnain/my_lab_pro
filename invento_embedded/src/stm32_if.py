#!/usr/bin/env python
import serial
import rospy
from std_msgs.msg import String, Bool, Int32
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
import math
import os
from geometry_msgs.msg import Point, PoseStamped, Pose, Quaternion
from tf.transformations import quaternion_from_euler, euler_from_quaternion
 



class stm32_if():
    def __init__(self):
        self.ros_init()
        self.serial_init()

    def ros_init(self):
        rospy.init_node("stm32_if", anonymous=True)
        self.rate = rospy.Rate(5)
        self.sub = rospy.Subscriber("arduino_in", String, callback=self.recv_cmd, queue_size=1)
        self.lidar_sub = rospy.Subscriber("lidar_running", Bool, callback=self.lidar_callback, queue_size=1)
        self.dest = rospy.Publisher('/socket/goalpose', PoseStamped, queue_size=1)


          

        #self.switch_to_mapping = rospy.Publisher('switch_to_mapping',String,queue_size=1)
        self.clear_map =rospy.Publisher('clear_map',String,queue_size=1)

        self.patrol_on_publisher = rospy.Publisher('pat_on', Int32, queue_size=1)
        self.patrol_off_publisher= rospy.Publisher('pat_off',Int32,queue_size=1)
        self.mark_patrol_point_roboremo= rospy.Publisher('mark_patrol_point_roboremo', PoseStamped, queue_size=1)

        self.start_recording_points_publisher=rospy.Publisher('start_recording_points',Int32,queue_size=1)
        self.stop_recording_points_publisher=rospy.Publisher('stop_recording_points',Int32,queue_size=1)
    
        self.marker_data=rospy.Publisher('marker_data',PoseWithCovarianceStamped, queue_size=1)

        self.current_pose_sub=rospy.Subscriber('amcl_pose',PoseWithCovarianceStamped, callback=self.get_curret_pose, queue_size=1)

        self.pub = rospy.Publisher("stm32_if/out", String, queue_size=1)
        self.cmd = None
        self.str_cmds=None
        self.lidar_running = False
        self.current_pose=PoseWithCovarianceStamped()
        self.x_offset=55000.0
        self.y_offset=65000.0
        self.z_offset=45000.0
        self.target_x_offset = 15000
        self.target_y_offset = 25000
        self.target_angle_offset = 35000

        self.stop_command = "403"
        self.nav_x = None
        self.nav_y = None
        self.nav_angle = None

        self.p = PoseStamped() 


    def serial_init(self):
        port = "/dev/motorcontrol"
        baud = 115200 
        timeout = 0.1

        self.ser = None
        while(self.ser is None):
            try:
                self.ser = serial.Serial(port, baud, timeout=timeout)
            except Exception as e:
                print(e)
                self.ser = None

    def recv_cmd(self, data):
        self.cmd = data.data

    def lidar_callback(self,data):
        if(data.data):
            self.lidar_running = "~901"
        else:
            self.lidar_running = "~900"

    def get_curret_pose(self,data):
        if data:
            #print('[INFO] Got Pose Data (but not capturing)')
            self.current_pose=data

    def send_serial(self):
        data = ""
        if(self.cmd):
            data += self.cmd
            self.cmd = None
        if(data):
            self.ser.write(data+'\r\n')
           
    def send_serial_encoded(self):
        data = ""
        if(self.str_cmds):
            data += self.str_cmds
            self.str_cmds = None
        if(data):
            self.ser.write(data.encode('utf-8'))
           
    def pwc2ps(self, pose_with_covariance):
        
        poseStamped = PoseStamped()
        poseStamped.header=pose_with_covariance.header
        poseStamped.pose.position.x = pose_with_covariance.pose.pose.position.x
        poseStamped.pose.position.y = pose_with_covariance.pose.pose.position.y
        poseStamped.pose.position.z = pose_with_covariance.pose.pose.position.z
        
        poseStamped.pose.orientation.x = pose_with_covariance.pose.pose.orientation.x
        poseStamped.pose.orientation.y = pose_with_covariance.pose.pose.orientation.y
        poseStamped.pose.orientation.z = pose_with_covariance.pose.pose.orientation.z
        poseStamped.pose.orientation.w = pose_with_covariance.pose.pose.orientation.w
        return poseStamped

    def xya2ps(self,x,y,angle):
        pos_x = x
        pos_y = y
        pos_angle = angle

        pos_angle = math.radians(pos_angle)  # angle in rad
        q = Quaternion()
        q.x, q.y, q.z, q.w = quaternion_from_euler(0, 0, pos_angle)
        self.p.pose.position.x = pos_x
        self.p.pose.position.y = pos_y
        self.p.pose.position.z = 0
        self.p.pose.orientation = q       
        self.p.header.frame_id = "/map"
        now = rospy.get_rostime()
        self.p.header.stamp.secs=now.secs
        self.p.header.stamp.nsecs=now.nsecs


    def read_serial(self):
        line = self.ser.readline().decode().strip("~").strip("\n").strip("\r")
        for data in line.split("~"):
            self.pub.publish(data)
        if data:
            if int(data)==960:
                print('[INFO] Publishing Patrol ON: ',data)
                self.patrol_on_publisher.publish(1)
            
            elif int(data)==961:
                print('[INFO] Publishing Patrol OFF: ',data)
                self.patrol_off_publisher.publish(0)
            
            elif int(data)==962:
                print('[INFO] Publishing Start Recording points: ',data)
                self.start_recording_points_publisher.publish(1)
        
            elif int(data)==963:
                print('[INFO] Publishing Stop Recording points: ',data)
                self.stop_recording_points_publisher.publish(0)
            
            elif int(data)==964:
                print(self.current_pose)
                if self.current_pose:
                    print('[INFO] Setting up marker!')
                    self.marker_data.publish(self.current_pose)                   
                    str_x = str((self.x_offset) + int(self.current_pose.pose.pose.position.x*100))
                    str_y = str((self.y_offset) + int(self.current_pose.pose.pose.position.y*100))
                    str_z = str((self.z_offset) + int(self.current_pose.pose.pose.position.z))
                    rospy.logwarn("curr_x : {} , curr_y : {}, curr_z:{}".format(str_x,str_y,str_z))
                    self.str_cmds = 'X:' + str_x +'Y:'+ str_y
            elif int(data)==965:
                print(self.pwc2ps(self.current_pose))
                self.mark_patrol_point_roboremo.publish(self.pwc2ps(self.current_pose))
                self.str_cmds='Marking Patrol Point coming from RoboRemo'
                self.marker_data.publish(self.current_pose)
                rospy.logwarn("roboremo setting current point as patrol point \n x: {}, y: {}".format(self.current_pose.pose.pose.position.x, self.current_pose.pose.pose.position.y))

            elif int(data)==966:
                print('[INFO] Starting Mapping')
                #self.switch_to_mapping.publish('mapping')
                os.system("rostopic pub -1 /switch_to_mapping std_msgs/String 'mapping'")               

            elif int(data)==967:
                print('[INFO] Clearing the Map')                    
                self.clear_map.publish('clearmap')
            
    
            
            elif int(data)==970:
                print('[INFO] Starting Nav')
                os.system("rostopic pub -1 /switch_to_navigation std_msgs/String 'navigation'")
                              
            #p2p USING RoboRemo/ipad            
            elif 10000<int(data)<20000:
                self.nav_x =(float(data)-(self.target_x_offset))/100.0
                self.nav_x_update = True
                rospy.logwarn( "x : {}".format(self.nav_x))

            elif 20000<int(data)<30000:
                self.nav_y = (float(data)-(self.target_y_offset))/100.0
                self.nav_y_update = True
                rospy.logwarn( "y : {}".format(self.nav_y))


            elif 30000<int(data)<40000:
                self.nav_angle = float(data)-(self.target_angle_offset)
                self.nav_angle_update = True
                rospy.logwarn(self.nav_angle)
                rospy.logwarn( "angle : {}".format(self.nav_angle))


            elif int(data)==411 and self.nav_x_update and self.nav_y_update and self.nav_angle_update:
                #Transform the x,y,theta from robotremo to posetamped
                self.xya2ps(self.nav_x,self.nav_y,self.nav_angle)
                #publish the POSE to /socket/goalpose
                self.dest.publish(self.p)
                rospy.logwarn('Navigating to chosen point!  {}'.format(self.p))
                self.nav_x_update = False
                self.nav_y_update = False
                self.nav_angle_update = False

            


    def run(self):
        while not rospy.is_shutdown():
            self.read_serial()
            self.send_serial()
            self.send_serial_encoded()
            self.rate.sleep()
        #to stop after shutdown
        self.ser.write(self.stop_command + "\r\n")

if __name__ == '__main__':
    interface = stm32_if()
    interface.run()
