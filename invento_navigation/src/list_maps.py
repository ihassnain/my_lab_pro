#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String, Int32
import os
path = "/home/vishwas/catkin_ws/src/invento_custom_localization/invento_navigation/maps"
pub = rospy.Publisher('maplist', String, queue_size=10)


def init():
    rospy.init_node('maplist_node', anonymous=True)


def maplist_callback(data):
    if data.data == 1:
        rate = rospy.Rate(1)
        mylist = os.listdir(path)
        data = ", ".join(mylist)
        print data
        pub.publish(data)
        rate.sleep()
    else:
        print "Not valid input"


def subscribers():
    rospy.Subscriber("show_map_list", Int32, maplist_callback)


if __name__ == '__main__':
    try:
        init()
        subscribers()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
