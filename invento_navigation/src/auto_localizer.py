#!/usr/bin/env python
import serial
import rospy
from std_msgs.msg import String, Bool, Int32
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
import pickle
import os

class AutoLocalize():
    def __init__(self):
        self.ros_init()
        self.file_name = 'last_position.pkl'

    def ros_init(self):
        print('[INFO] Initizliaing ROS NODE :AUTO LOCALIZER')
        rospy.init_node("auto_localizer", anonymous=True)
        self.rate = rospy.Rate(5)
        self.current_pose_sub=rospy.Subscriber('amcl_pose',PoseWithCovarianceStamped, callback=self.get_current_pose, queue_size=1)
        rospy.spin()

    def get_current_pose(self,data):
        #store current pose locally
        if data:
            pickle.dump(data, open("last_position.pkl", "w"))
            print('[INFO] Pickled!')
        else:
            print('[INFO] No topic found AMCL')
            print('[Potential Error] Mapping mode on, amcl not avilable')

if __name__ == '__main__':
    try:
        pose=pickle.load(open("last_position.pkl"))
        if pose:
            print('[INFO] Last posisiton found Auto Localizing', pose)
            print(pose)
            for i in range(0,2):
                try:
                    os.system("rostopic pub -1 /initialpose geometry_msgs/PoseWithCovarianceStamped '{}'".format(pose))
                    print('[INFO] Re Localized')
                except:
                    print('[WARNING] Tried AUTO Localization Failed!')
        else:
            print('pose not found')

    except Exception as e:
        print(e)

    localizer = AutoLocalize()
    

