#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import rospy
from std_msgs.msg import String
from std_msgs.msg import String, Bool, Int32
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
from nav_msgs.msg import Odometry, Path
import os
from sensor_msgs.msg import LaserScan
from tf import transformations
from scipy.spatial import distance
import math
import time
from threading import Thread
import signal
import sys

path=[]
obstacle_detection_enabled = True
update_obstacle_flag = False
debug = False
target = None

#path params
angular_tolerance  = 2
linear_tolerance   = 0.5
path_accepting_tol = 0.8
#path_skip_value = 0.65
path_skip_value = 1.0
final_angle  = None
final_angle_tol = 10
current_heading = 0.0
#obtacle params
obstacle = False
front_obs_range = 0.6
side_obs_range = 0.3
flag_moving = False
scaling_factor = 5
#pwm values

default_left_pwm = 515
default_right_pwm = 615

default_left_turning = 507
default_right_turning = 607


"""
input: data you want to send to arduino
Result : Publishes the data to the arduino 
"""

arduino_pub = rospy.Publisher('arduino_in', String, queue_size=1)

def send(data):
    cmd = ""
    for i in data:
        cmd += "~" + str(i)
    #print(cmd)
    
    arduino_pub.publish(cmd)


"""
Takes the direction and angle as input 
and decides what pwm to apply and should also update the target point to the next target point
"""

def cal_angle_diff(curr, target):
    if(curr > target):
        direction = -1                                                                    
        curr, target = target, curr
    else:
        direction = 1
    
    diff1 = abs(target-curr)
    diff2 = 360-diff1
    
    if(diff1 <= diff2):
        return diff1*direction
    else:
        return -1*diff2*direction



def apply_pwm(direction,correction_angle):
    global angular_tolerance, scaling_factor
    #print('Correction Suggestion',direction)
    if int(correction_angle) > angular_tolerance:
        if direction == 'anti_clockwise':
            if  correction_angle >= 40:
                #set left pwm zero (600)
                #set right pwm 10  (510)
                #print('Left: ',600,'Right: ',510)
                send([607, 507,402])
                
            if correction_angle < 40:
                # Apply division by 5 and subtract quotient from left and add quotient to right
                """
                example : correction angle = 30
                quotient = 30/5 =6
                left = 614-6 = 608 (614 default left)
                right= 514+6 = 520 (514 default right)
                """
                quotient = int(correction_angle/scaling_factor)
                #print('Left: ',614-quotient,'Right: ',514+quotient)
                send([614-quotient,514+quotient,401])
                        
            
        if direction == 'clockwise':
            if correction_angle >= 40 :
                #set right pwm zero
                #set left pwm 10
                #print('Left: ',610,'Right: ',500)
                send([607,507,404])
               
            if correction_angle < 40:
                #apply division by 5 and subtract quotient from right and add the quotient to left
                quotient = int(correction_angle/scaling_factor)
                send([614+quotient,514-quotient,401])
                #print('Left: ',614+quotient,'Right: ',514-quotient)
                 
    else:
        #print('Left: ',614,'Right: ',514)
        send([614,514,401])
    
"""
This function takes two points as unit vectors and gives the cos of the angle between them
Input: p2, p1 points
Output: the cosine inverse i.e angle in degrees
"""
def get_cosine_angle(p2,p1):
	numerator = float(p2[0]-p1[0])
	denominator = (p2[0]-p1[0])**2 + (p2[1]-p1[1])**2
	denominator  = float(math.sqrt(denominator))
	return (math.acos(float(numerator/denominator))*180)/math.pi


def get_turning_angle(p2,p1,current_heading):
    
    #print('Target',p2,'current',p1,current_heading)
    current_heading = current_heading * 0.0174533
    numerator = ((p2[0]-p1[0]) * math.cos(current_heading)) + ((p2[1]-p1[1])*math.sin(current_heading))   
    denominator = (p2[0]-p1[0])**2 + (p2[1]-p1[1])**2
    denominator  = float(math.sqrt(denominator))
    return (math.acos(float(numerator/denominator))*180)/math.pi



def path_callback(data):
    global path, final_angle,flag_moving
    path = []
    for point in data.poses:
        x = point.pose.position.x
        y = point.pose.position.y
        path.append([x, y])

    final_angle = quaternion_to_euler(data.poses[-1].pose)[2]
    flag_moving = True


def reached_final_point(current):
    global path,path_accepting_tol
    if distance.euclidean(current,path[-1])<=path_accepting_tol:
        return True
    else:
        return False
  

def get_waypoint(current):
    global path    
    for target in path:
        if distance.euclidean(current,target)> path_skip_value:
            path.pop(0)
            return target
        path.pop(0)
    send([600,500,403])
    return None


def circular_error(curr, target):
    e = abs(curr - target)
    if(curr > target):
        if(e > 180):
            return 360 - e
        else: 
            return -e
    else:
        if(e > 180):
            return e - 360
        else:
            return e
def ack_to_ipad(data):
    print("sending to ipad : {}".format(data))
    

def head_to_final_angle():                                                                                                                                                                               
    global current_heading,final_angle, final_angle_tol, flag_moving
    correction=circular_error(current_heading,final_angle)
    if abs(correction) >= final_angle_tol:
        if correction >= 0:
            send([default_left_turning,default_right_turning,402])
            #apply_pwm('anti_clockwise',abs(correction))
        else :
            send([default_left_turning,default_right_turning,404])
            #apply_pwm('clockwise',abs(correction))
    else: 
        print("reached final angle")
        flag_moving = False
        send([default_left_pwm,default_right_pwm,403])
        ack_to_ipad("111")


def head_to_target(x1,y1,x2,y2,current_heading,desired_heading,correction_angle):
    global target
    if y1 < y2 and current_heading <= 180:
        if current_heading>=desired_heading:
            apply_pwm('clockwise',correction_angle)
        if current_heading<desired_heading:
            apply_pwm('anti_clockwise',correction_angle)
                    
    elif y1 < y2 and current_heading > 180:
        if current_heading-desired_heading <= 180:
            apply_pwm('clockwise',correction_angle)
        if current_heading-desired_heading>180:
            apply_pwm('anti_clockwise',correction_angle)
            
    elif y1>y2 and current_heading<180:
        if 360-desired_heading-current_heading <= 180:
            apply_pwm('anti_clockwise',correction_angle)
        if 360-desired_heading-current_heading > 180:
            apply_pwm('clockwise',correction_angle)

    elif y1>y2 and current_heading>=180:
        if current_heading <= 360-desired_heading:
            apply_pwm('anti_clockwise',correction_angle)
        if current_heading > 360-desired_heading:
            apply_pwm('clockwise',correction_angle)

    elif y1==y2 and x1>x2:
        if current_hading<=180:
            apply_pwm('anti_clockwise',correction_angle)
        if current_heading>180:
            apply_pwm('clockwise',correction_angle)
                                                                                                                                                                        
    elif y1==y2 and x1<x2:
        if current_heading<=180:
            apply_pwm('clockwise',correction_angle)
        if current_heading>180:
            apply_pwm('anti_clockwise',correction_angle)     


    if distance.euclidean([x1,y1],[x2,y2])<linear_tolerance:
        target = None

def navigate():
    pass
"""
ROS Callback function for the robot_odom topic.
Whenever there is a change in the robot position, it recalculates its
orientation with the target and looks to correct its course! 
"""
def get_current_pose(data):
    global target, obstacle, start_time, end_time, current_heading,flag_moving
    curr_x,curr_y,current_heading = quaternion_to_euler(data.pose.pose)
    current=[curr_x,curr_y]
    if flag_moving:
        if target and not obstacle:
            desired_heading = get_cosine_angle(target,current)
            y2,y1 = target[1],current[1]
            x2,x1 = target[0],current[0]
            correction_angle = get_turning_angle(target,current,current_heading)
            head_to_target(x1,y1,x2,y2,current_heading,desired_heading,correction_angle)
        else:
            if path:
                if reached_final_point(current):
                    head_to_final_angle()
                    #print("reached destination")
                else:
                    target = get_waypoint(current)
            else:
                pass
                #no path no target

    else:
        pass 
        #not moving
        

    
def quaternion_to_euler(pose):
    quaternion = [pose.orientation.x, pose.orientation.y,pose.orientation.z, pose.orientation.w]
    euler = transformations.euler_from_quaternion(quaternion)
    x = pose.position.x
    y = pose.position.y
    a = euler[2]*180/math.pi % 360
    return x,y,a


def scan_callback(data):
    global obstacle, dist, start_time, end_time, obstacle_detection_enabled, update_obstacle_flag
    if obstacle_detection_enabled == True:
            dist_range = data.ranges
            if min(data.ranges[0:25])<front_obs_range:
                obstacle = True
            elif min(data.ranges[25:52])<side_obs_range:
                obstacle = True
            elif min(data.ranges[320:340])<side_obs_range:
                obstacle = True
            elif min(data.ranges[340:360])<front_obs_range:
                obstacle = True
            else:
                obstacle = False
            if obstacle == True:
                print('Obstacle detected')
                send([600,500,403])

def handler(signum ,frame):
    send([614, 514,403])
    print("Sending stop once node is killed")
    sys.exit()

def position():
    rospy.init_node('navigator_test_node', anonymous=True)
    print('Node Intialized : navigation_test_node')
    rospy.Subscriber('robot_odom',Odometry, callback=get_current_pose, queue_size=1)
    rospy.Subscriber('/move_base/NavfnROS/plan',Path, callback=path_callback, queue_size=1)
    rospy.Subscriber('/scan',LaserScan,callback=scan_callback,queue_size=1)
    signal.signal(signal.SIGINT, handler)
    rospy.spin()


if __name__ == '__main__':
    position()

