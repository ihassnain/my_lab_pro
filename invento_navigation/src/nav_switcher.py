#!/usr/bin/env python

import os
import rospy
from std_msgs.msg import String


def switch_to_mapping_callback(msg):
    if (msg.data == "mapping"):
         os.system("rosnode kill map_server")
         os.system("rosnode kill turtle_tf_listener")
        #  os.system("rosnode kill odom_publisher")
         os.system("rosnode kill amcl")
         os.system("rosnode kill markers_node")
         print('Switched to mapping')
         os.system("roslaunch invento_navigation mapping.launch")
         os.system("roslaunch invento_navigation odom_related.launch")
        
def clear_map_callback(msg):
    if (msg.data=="clearmap"):
         print('[INFO] CLEARING THE MAP RECIVED COMMAND!')
         os.system("rostopic pub -1 /syscommand std_msgs/String 'data: 'reset''")
         
def switch_to_navigation_callback(msg):
    print('Entering navigation callback')
    if (msg.data=="navigation"):
        os.system("rosnode kill gmapping")
        # os.system("rosnode kill odom_publisher")
        print('Killed Gmapping')
        os.system("source .mapconfig")
        os.system("roslaunch invento_navigation localization_and_navigation.launch")
        os.system("rostopic pub -1 /load_latest_map std_msgs/String 'data: 'latest_map''") 
        os.system("rosrun invento_description markers.py")
        os.system("rosrun tf static_transform_publisher 0 0 0 0 0 0 map marker_frame 100")
 
def switcher_node():
    rospy.init_node("nav_switcher")
    rospy.Subscriber('switch_to_mapping', String, switch_to_mapping_callback, queue_size=1)
    rospy.Subscriber('clear_map', String, clear_map_callback, queue_size=1)
    rospy.Subscriber('switch_to_navigation', String, switch_to_navigation_callback, queue_size=1)
    rospy.spin()

if __name__ == '__main__':
    switcher_node()
