#!/usr/bin/env python

from nav_msgs.msg import OccupancyGrid, Path, Odometry
from geometry_msgs.msg import PoseStamped
import rospy
import heapq
import cv2
import numpy as np

rate = None
occ_map = None
path_publisher = None
detailed_path_publisher = None

resolution = 0
robot_resolution = 0
origin_x = 0
origin_y = 0

curr_pos = [0, 0]
target_pos = [0, 0]

curr_grid = (0, 0)
target_grid = (0, 0)

robot_radius = 0.9
max_x = 0
max_y = 0
img = None
dist_tol = 4


class Node:
    def __init__(self, parent=None, curr_pos=(0, 0), target_pos=(0, 0)):
        self.increment = 4
        self.turn_weight = 16
        self.parent = parent
        self.curr_pos = curr_pos
        self.target_pos = target_pos
        self.children = []
        self.valid = self.is_valid()
        self.turn = self.is_turning()
        self.cost = self.calculate_cost()

    def is_valid(self):
        for x in range(-robot_resolution, robot_resolution+1):
            for y in range(-robot_resolution, robot_resolution+1):
                check_x = self.curr_pos[0] + x
                check_y = self.curr_pos[1] + y
                index = max_x * check_y + check_x
                if(check_x <= 0 or check_x >= max_x or check_y <= 0 or check_y >= max_y or occ_map[index] == 100 or occ_map[index] == -1):
                    return False
        return True

    def expand(self):
        children_pos = [
            (self.curr_pos[0]+self.increment, self.curr_pos[1]),
            (self.curr_pos[0]-self.increment, self.curr_pos[1]),
            (self.curr_pos[0], self.curr_pos[1]+self.increment),
            (self.curr_pos[0], self.curr_pos[1]-self.increment)
        ]

        for child_pos in children_pos:
            child = Node(parent=self, curr_pos=child_pos,
                         target_pos=self.target_pos)
            self.children.append(child)

    def calculate_cost(self):
        heuristic = ((self.curr_pos[0] - self.target_pos[0]) **
                     2 + (self.curr_pos[1] - self.target_pos[1])**2)**0.5
        actual = self.turn
        if(self.parent is not None):
            actual += self.parent.cost[1]
        return [heuristic, actual]

    def is_turning(self):
        if(self.parent is None or self.parent.parent is None):
            return 0

        child_pos = self.curr_pos
        grandparent_pos = self.parent.parent.curr_pos

        if(grandparent_pos[0] == child_pos[0] or grandparent_pos[1] == child_pos[1]):
            return 0
        return self.turn_weight


def search(curr_pos, target_pos):
    global img, reached, dist_tol

    level = 0
    current_node = Node(curr_pos=curr_pos, target_pos=target_pos)
    visited = set(current_node.curr_pos)
    queue = [(0, 0, current_node)]
    reached = False

    def geodiff():
        dist = ((target_pos[0]-current_node.curr_pos[0])**2 +
                (target_pos[1]-current_node.curr_pos[1])**2) ** 0.5
        print dist
        return dist

    while current_node.curr_pos != target_pos and len(queue) != 0 and not reached:
        current_node = heapq.heappop(queue)[2]
        current_node.expand()

        for child in current_node.children:

            def geodiff():
                dist = ((target_pos[0]-child.curr_pos[0])**2 +
                        (target_pos[1]-child.curr_pos[1])**2) ** 0.5

                # print dist
                return dist

            # if(child.curr_pos == target_pos):
            if(geodiff() <= dist_tol):
                current_node = child
                reached = True
                current_node.curr_pos != target_pos
                print("Found node")
                break

            if(child.valid and child.curr_pos not in visited):
                queue_item = (sum(child.cost), level, child)
                heapq.heappush(queue, queue_item)
                visited.add(child.curr_pos)
                #img[child.curr_pos[0]][child.curr_pos[1]][2] = 255

                #cv2.imshow("image", img)
                # cv2.waitKey(1)

        level += 1
        # print "level up {}".format(level)

    path = []
    detailed_path = []

    if(reached):
        path.append(current_node.curr_pos)
        while(current_node.parent != None):
            if(current_node.turn):
                path.append(current_node.parent.curr_pos)
            detailed_path.append(current_node.parent.curr_pos)
            current_node = current_node.parent

        path.append(curr_pos)
        path.reverse()

        detailed_path.append(curr_pos)
        detailed_path.reverse()

    return path, detailed_path


def get_map(data):
    global occ_map, resolution, max_x, max_y, robot_resolution, img, origin_x, origin_y

#    print("Getting map")
    occ_map = data.data
    resolution = data.info.resolution
    robot_resolution = int(robot_radius/resolution)
    max_x = data.info.width
    max_y = data.info.height
    origin_x = data.info.origin.position.x
    origin_y = data.info.origin.position.y

    #img = np.zeros((max_x, max_y, 3), np.uint8)

    # for i in range(0, max_x):
    #    for j in range(0, max_y):
    #        if data.data[max_x*j + i] == -1:
    #            img[i][j][0] = 255
    #            img[i][j][1] = 255
    #            img[i][j][2] = 255
    #        if data.data[max_x*j + i] == 0:
    #            img[i][j][1] = 255
    #        if data.data[max_x*j + i] == 100:
    #            img[i][j][0] = 0
    #            img[i][j][1] = 0
    #            img[i][j][2] = 0


def get_plan(data):
    global target_grid, target_pos, resolution, origin_x, origin_y, curr_grid

    x, y = data.pose.position.x-origin_x, data.pose.position.y-origin_y

    target_pos = data
    #target_pos.pose.position.x *= -1
    #target_pos.pose.position.y *= -1

    #print "Target point x: {}, y: {}".format(x, y)
    target_grid = (int(x/resolution), int(y/resolution))

    print "Target grid x: {}, y: {}".format(target_grid[0], target_grid[1])
    #img[target_grid[0]][target_grid[1]][0] = 255

    points, detailed_points = search(curr_grid, target_grid)
    if(len(points) == 0):
        print("Empty path")
        return
    print(points)

    path = Path()
    detailed_path = Path()

    path.header.frame_id = "map"
    detailed_path.header.frame_id = "map"

    for point in points:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = (point[0]*resolution + origin_x)
        pose.pose.position.y = (point[1]*resolution + origin_y)
        path.poses.append(pose)
    path.poses[-1].pose.orientation = target_pos.pose.orientation

    for point in detailed_points:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = (point[0]*resolution + origin_x)
        pose.pose.position.y = (point[1]*resolution + origin_y)
        detailed_path.poses.append(pose)
    detailed_path.poses[-1].pose.orientation = target_pos.pose.orientation

    path.poses[-1].pose.orientation = target_pos.pose.orientation
    path_publisher.publish(path)

    detailed_path.poses[-1].pose.orientation = target_pos.pose.orientation
    detailed_path_publisher.publish(detailed_path)


def position_callback(data):
    global curr_pos, curr_grid, resolution, origin_x, origin_y
    if(resolution == 0):
        return
    x, y = data.pose.pose.position.x - origin_x, data.pose.pose.position.y - origin_y
    curr_pos = [x, y]
    #print "Current point x: {}, y: {}".format(x,y)
    curr_grid = (int(x/resolution), int(y/resolution))


def create_path():
    global rate, path_publisher, detailed_path_publisher

    rospy.init_node('global_planner', anonymous=True)
    path_publisher = rospy.Publisher("/global_plan_path", Path, queue_size=1)
    detailed_path_publisher = rospy.Publisher(
        "/global_plan_path_detailed", Path, queue_size=1)

    rospy.Subscriber("/map", OccupancyGrid, get_map, queue_size=1)
    rospy.Subscriber("/socket/goalpose", PoseStamped, get_plan, queue_size=1)
    rospy.Subscriber("/robot_odom", Odometry, position_callback, queue_size=1)

    rate = rospy.Rate(10)
    rospy.spin()


if __name__ == "__main__":
    create_path()
