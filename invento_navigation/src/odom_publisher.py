#!/usr/bin/env python

import math
from math import sin, cos, pi

import rospy
import tf
from nav_msgs.msg import Odometry
from std_msgs.msg import Int64MultiArray
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3


class OdometryPublisher():
    def __init__(self, encoder_topic, ticks_per_rotation, wheel_to_wheel_distance, wheel_radius):
        rospy.init_node("odom_publisher")

        rospy.Subscriber(encoder_topic, Int64MultiArray,
                         self.encoder_ticks_callback, queue_size=50)

        self.odom_pub = rospy.Publisher(
            "calculated_odom", Odometry, queue_size=50)
        self.odom_broadcaster = tf.TransformBroadcaster()

        self.ticks_per_rotation = encoder_ticks_per_rotation
        self.wheel_to_wheel_distance = wheel_to_wheel_distance
        self.wheel_radius = wheel_radius

        self.x = 0
        self.y = 0
        self.theta = 0

        self.vel_x = 0
        self.vel_y = 0
        self.vel_theta = 0
        self.rate = rospy.Rate(10)

	self.base_link = "base_link"

        self.last_time = rospy.Time.now()
        while not rospy.is_shutdown():
            self.publish_odom()
            self.rate.sleep()

    def encoder_ticks_callback(self, msg):
        current_time = rospy.Time.now()
        time_diff = (current_time - self.last_time).to_sec()
        self.last_time = current_time

        left_delta, right_delta = msg.data

        left_diff = (2 * pi * self.wheel_radius * left_delta) / \
            self.ticks_per_rotation
        right_diff = (2 * pi * self.wheel_radius * right_delta) / \
            self.ticks_per_rotation

        if (time_diff == 0):
            return

        vel_left = left_diff/time_diff
        vel_right = right_diff/time_diff

        theta_diff = ((vel_right - vel_left)*time_diff) / \
            self.wheel_to_wheel_distance
        self.vel_theta = theta_diff/time_diff

        diff = vel_right - vel_left
        summation = vel_right + vel_left

        if diff == 0:
            x_diff = summation * time_diff * cos(self.theta)/2.0
            y_diff = summation * time_diff * sin(self.theta)/2.0
        else:
            x_diff = self.wheel_to_wheel_distance * summation * \
                (sin(theta_diff + self.theta) -
                 sin(self.theta))/(2.0 * diff)
            y_diff = -1 * self.wheel_to_wheel_distance * summation * \
                (cos(theta_diff + self.theta) -
                 cos(self.theta))/(2.0 * diff)

        self.vel_x = x_diff/time_diff
        self.vel_y = y_diff/time_diff

        self.x += x_diff
        self.y += y_diff
        self.theta = (self.theta+theta_diff) % (2 * pi)

    def publish_odom(self):
        odom_quat = tf.transformations.quaternion_from_euler(0, 0, self.theta)

        self.odom_broadcaster.sendTransform(
             (self.x, self.y, 0.035),
             odom_quat,
             rospy.Time.now(),
             self.base_link,
             "calculated_odom"
         )

        odom = Odometry()
        odom.header.stamp = rospy.Time.now()
        odom.header.frame_id = "calculated_odom"
        odom.pose.pose = Pose(Point(self.x, self.y, 0.035),
                              Quaternion(*odom_quat))

        odom.twist.twist = Twist(
            Vector3(self.vel_x, self.vel_y, 0), Vector3(0, 0, self.vel_theta))

        odom.child_frame_id = self.base_link

        self.odom_pub.publish(odom)


if __name__ == "__main__":
    encoder_topic = "/encoder_ticks"
    encoder_ticks_per_rotation = rospy.get_param("encoder/ticks_per_rotation")
    wheel_to_wheel_distance = rospy.get_param(
        "encoder/base_width")
    wheel_radius = rospy.get_param("encoder/wheel_radius")
    odom = OdometryPublisher(
        encoder_topic, encoder_ticks_per_rotation, wheel_to_wheel_distance, wheel_radius)
