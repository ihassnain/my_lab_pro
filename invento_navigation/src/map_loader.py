#!/usr/bin/env python

import rospy
import rospkg
import os
from std_msgs.msg import String
# The idea is to have to have both Load latest map and Load map given a name

def load_latest_map_callback(data):
    print(data)
    if data.data == 'latest_map':
        os.system('rosnode kill map_server')
        mapfilename = rospy.get_param('latest_map_file')         # Get map name from param server 
        foldername =  rospkg.RosPack().get_path('invento_navigation')
        filepath = os.path.join(foldername, "maps", mapfilename)
        os.system('rosrun map_server map_server {:s}'.format(filepath))
        
def available_in_maplist(map_name):
    foldername = rospkg.RosPack().get_path('invento_navigation')
    mapfolder = os.path.join(foldername, 'maps')
    maps = os.listdir(mapfolder)
    pklsonly = [e for e in maps if e.endswith('.yaml')]
    removeext = [ e.replace(".yaml","") for e in pklsonly ]
    if map_name in removeext:
        return True
    else:
        return False

def load_new_map_callback(data):
    print(data) # data is going to be the name of the map without '.yaml'
    #map_name = data.data
    map_name = "raushan" 
    if available_in_maplist(map_name):
        rospy.logwarn("loading map")
        print('[INFO] Loading new map!')
        os.system('rosnode kill map_server')
        mapfilename = data.data + '.yaml'
        foldername =  rospkg.RosPack().get_path('invento_navigation')
        filepath = os.path.join(foldername, "maps", mapfilename)
        os.system('rosrun map_server map_server {:s}'.format(filepath))

    else:
        # rospy.logwarn('Map does not exist!)
        print('Map does not exist!')
    

if __name__ == '__main__':
  rospy.init_node('map_loader')
  rospy.Subscriber('load_latest_map', String, load_latest_map_callback)
  rospy.Subscriber('load_new_map', String, load_new_map_callback)
  rospy.spin()

