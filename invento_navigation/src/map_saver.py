#!/usr/bin/env python                                                                                                                                                                                          

import rospy
import rospkg
import os
from std_msgs.msg import String

def get_full_map_path(map_name):
    filename = "{:s}".format(map_name)
    foldername =  rospkg.RosPack().get_path('invento_navigation')
    filepath = os.path.join(foldername, "maps", filename)
    return filepath

def write_mapconfig(map_name):
    # save to environment variable
    rospy.logwarn("Writing new map file name to map config file")
    with open(os.path.join(os.path.expanduser("~"), ".mapconfig"), "w") as mpf:
        mpf.write('export MAPFILE="{:s}.yaml"'.format(map_name))

def available_in_maplist(map_name):
    foldername = rospkg.RosPack().get_path('invento_navigation')
    mapfolder = os.path.join(foldername, 'maps')
    maps = os.listdir(mapfolder)
    pklsonly = [e for e in maps if e.endswith('.yaml')]
    removeext = [ e.replace(".yaml","") for e in pklsonly ]
    if map_name in removeext:
        rospy.logwarn('Map already exists')
    else:
        write_mapconfig(map_name)
        get_map(map_name)
        set_latest_map_param(map_name)

def get_map(map_name):
    os.system('rosrun map_server map_saver -f {:s}'.format(get_full_map_path(map_name)))
    print('Map saved!')

def map_name_callback(data):
    map_name = data.data
    available_in_maplist(map_name)
    
def set_latest_map_param(map_name):
    # Set the latest map file name parameter
    map_name = map_name + '.yaml'
    rospy.set_param('latest_map_file', map_name)    

if __name__ == '__main__':
    rospy.init_node('map_saver', anonymous=True)
    rospy.Subscriber('map_saver', String, map_name_callback)
    # map_name = rospy.get_param('~map_name')
    rospy.spin()
